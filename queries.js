const db = require('./dbcon');

// untuk enkripsi
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const getUsers = (request, response) => {
	db.query('SELECT * FROM users ORDER BY user_id ASC', (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const getUserById = (request, response) => {
	const id = parseInt(request.params.id);

	db.query('SELECT * FROM users WHERE user_id = $1', [id], (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const createUser = (request, response) => {
	const { name, email, password } = request.body;

	db.query(
		'INSERT INTO users (name, email, password) VALUES ($1, $2, $3) RETURNING *',
		[name, email, password],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(201).send(`User added with ID: ${results.rows[0].id}`);
		}
	);
};

const updateUser = (request, response) => {
	const id = parseInt(request.params.id);
	const { name, email } = request.body;

	db.query(
		'UPDATE users SET name = $1, email = $2 WHERE id = $3',
		[name, email, id],
		(error, results) => {
			if (error) {
				throw error;
			}
			response.status(200).send(`User modified with ID: ${id}`);
		}
	);
};

const deleteUser = (request, response) => {
	const id = parseInt(request.params.id);

	db.query('DELETE FROM users WHERE user_id = $1', [id], (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).send(`User deleted with ID: ${id}`);
	});
};

const regUser = (request, response) => {
	console.log("regUser", request.body);

	const { name, email, password } = request.body;
	db.query(
		`SELECT * FROM users WHERE email = $1`,
		[email],
		(err, result) => {
			if (result.rows.length > 0) {
				response.status(400).send({
					msg: 'This user is already registered!'
				});
				console.log('This user is already registered!')
			} else {
				// username is available
				bcrypt.hash(password, 10, (err, hash) => {
					if (err) {
						response.status(500).send({
							msg: err
						});
					} else {
						// has hashed pw => add to database
						db.query(
							`INSERT INTO users (name, email, password) VALUES ($1, $2, $3)`, 
							[name, email, hash],
							(err, result) => {
								if (err) {
									throw err;
									response.status(400).send({
										msg: err
									});
									console.log('Error input to db')
								}
								console.log('The user is successfully registered!')
								response.status(201).send({
									msg: 'The user is successfully registered!'
								});
							}
						);
					}
				});
			}
		}
	);
};

const loginUser = (request, response) => {
	console.log("loginUser", request.body);

	const { email, password } = request.body;

	db.query(
		`SELECT * FROM users WHERE email = $1`,
		[email],
    (err, result) => {
      // user does not exists
      if (err) {
        throw err;
        return response.status(400).send({
          msg: err
        });
      }

      if (result.rowCount != 1) {
        return response.status(401).send({
          msg: 'Email or password is incorrect!'
        });
      }
      // check password
      const theUser = result.rows[0];
      bcrypt.compare(
        request.body.password,
        theUser.password,
        (bErr, bResult) => {
          // wrong password
          if (bErr) {
            throw bErr;
            return response.status(401).send({
              msg: 'Email or password is incorrect!'
            });
          }
          if (bResult) {
            const token = jwt.sign({
              id: theUser.user_id,
              user: theUser
            }, 'the-super-strong-secret', {
              expiresIn: '1h'
            });
            db.query(
              `UPDATE users SET last_login = now() WHERE user_id = $1`,
              [theUser.user_id]
            );
            return response.status(200).send({
              msg: 'Logged in!',
              token,
              user: theUser
            });
          }
          return response.status(401).send({
            msg: 'Username or password is incorrect!'
          });
        }
      );
    }
  );
};

const getArticleCategories = (request, response) => {
	db.query('SELECT * FROM categories ORDER BY category_id ASC', (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const createArticle = (request, response) => {
	console.log("request.authorization ", request);
	if (
    !request.headers.authorization ||
    !request.headers.authorization.startsWith('Bearer') ||
    !request.headers.authorization.split(' ')[1]
  ) {
    return response.status(422).json({
      message: "Please provide the token",
    });
  }

	const { title, short_description, description, category_id, is_visible, image } = request.body;

	const theToken = request.headers.authorization.split(' ')[1];
	try {
		var decoded = jwt.verify(theToken, 'the-super-strong-secret');

		db.query(
			'INSERT INTO articles (title, short_description, description, category_id, is_visible, image) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *',
			[title, short_description, description, category_id, is_visible, image],
			(error, results) => {
				if (error) {
					throw error;
				}
				response.status(201).send({'msg':'berhasil create article'});
			}
		);
	} 
	catch(err) { // err
		return response.status(422).json({
      message: "Invalid token",
    });
	}
};

const getArticles = (request, response) => {
	db.query('SELECT * FROM articles ORDER BY article_id DESC', (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const getArticlesInLimit = (request, response) => {
	const limit = parseInt(request.params.limit);

	db.query('SELECT * FROM articles ORDER BY article_id DESC LIMIT $1', [limit], (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const getArticleDetail = (request, response) => {
	const id = parseInt(request.params.id);
	console.log("id",id)

	db.query('SELECT * FROM articles WHERE article_id = $1', [id], (error, results) => {
		if (error) {
			throw error;
		}
		response.status(200).json(results.rows);
	});
};

const logoutUser = (request, response) => {
	response.status(201).send({
		msg: 'Logout is done by the NuxtJS client, using auth.logout.'
	});
};

module.exports = {
	getUsers,
	getUserById,
	createUser,
	updateUser,
	deleteUser,

	regUser,
	loginUser,
	getArticleCategories,
	createArticle,
	getArticles,
	getArticlesInLimit,
	getArticleDetail,
	logoutUser
};
