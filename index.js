const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 4000;

const db = require('./queries');

// untuk validasi
const {
  signupValidation,
  loginValidation
} = require('./validation');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST");
  res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");
  next();
});

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.get('/', (request, response) => {
  response.json({ info: 'Node.js, Express, and Postgres API' });
});

// API ori
app.get('/users', db.getUsers);
app.get('/users/:id', db.getUserById);
app.post('/users', db.createUser);
app.put('/users/:id', db.updateUser);
app.delete('/users/:id', db.deleteUser);

// API sahaware
app.post('/register', signupValidation, db.regUser);
app.post('/login', db.loginUser);
app.get('/categories', db.getArticleCategories);
app.post('/create_article', db.createArticle);
app.get('/articles', db.getArticles);
app.get('/articles/:limit', db.getArticlesInLimit);
app.get('/article/:id', db.getArticleDetail);
app.get('/logout', db.logoutUser);

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
